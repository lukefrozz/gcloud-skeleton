# Flask-Skeleton
Projeto base para backend Python+PostgreSQL com autenticação

## Virtualenv
1. O projeto funciona perfeitamente com o Python 3.7, para criar uma nova virtualenv no linux ou mac use o comando:
`python3.7 -m venv venv`
2. Após criar o projeto, entre na virtualenv através do comando: 
`source venv/bin/python`  
e instale as dependências com o comando: `pip install -r requirements.txt`

> No Windows, por usar o PyCharm, eu crio uma virtualenv direto na IDE ou recorro ao WSL

## Banco de dados
Para configurar o banco crie um usuário no PostgreSQL com a senha que desejar e mude o arquivo `config.py` com o usuário, senha e nome do banco na `linha 7`

Após isso, execute o arquivo `manage.py` com os seguintes comandos: 
`./manage.py db init`, `./manage.py db migrate` e `./manage.py db upgrade`. 
Desta forma o banco de dados será corretamente inicializado

> Para atualizar alguma alteração no banco basta executar `./manage.py db migrate` e `./manage.py db upgrade` novamente.

> O comando `db init` configura os arquivos do alembic pra automatizar a migração.

> O comando `db migrate` cria um novo arquivo de migração a ser executado para modificar o banco.

> O comando `db upgrade` altera o banco com base no último arquivo de migração gerado.
> Fique atento a uma possível falha na migração, pode ser que você tenha que alterar a ordem de alteração para funcionar,
> raramente acontece, porém é possível, como já aconteceu em casos onde o arquivo estava gerando uma chave estrangeira antes
> de gerar a tabela no banco.

## Iniciar o serviço local
Caso esteja usando o PyCharm, basta iniciar o debug no arquivo `run_debug.py` pressionando `Shift+F9` ou `Shift+fn+F9` dependendo da configuração do seu teclado. 
Se estiver pelo terminal, execute o comando `./manage.py runserver` que o resultado será semelhante.

> Recomendo o uso de uma IDE para facilitar o desenvolvimento com o uso de breakpoints,
> não é algo obrigatório, mas faz parte da minha metodologia de trabalho.