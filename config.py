import os
from datetime import timedelta


DEBUG = True
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_DATABASE_URI = 'postgres://lanchonete:lanchonete@localhost:5432/db_lanchonete'
SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_ECHO = False
JWT_EXPIRATION_DELTA = timedelta(hours=10)
JWT_ALGORITHM = 'HS512'
JWT_VERIFY_CLAIMS = ['signature', 'exp', 'nbf', 'iat']
JWT_REQUIRED_CLAIMS = ['exp', 'iat', 'nbf']
THREADS_PER_PAGE = 2
CSRF_ENABLED = True
CSRF_SESSION_KEY = "zafenate"
SECURITY_PASSWORD_HASH = "pbkdf2_sha512"
SECRET_KEY = "zafenate"
UPLOAD_FOLDER = os.path.join(BASE_DIR, 'app/static/uploads')
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}
# email server
MAIL_SERVER = 'smtp.yoursite.com'
MAIL_PORT = 587
MAIL_DEBUG = True
MAIL_USE_TLS = True
MAIL_USE_SSL = False
MAIL_USERNAME = 'noreply@yoursite.com'
MAIL_PASSWORD = 'no@reply1290'
