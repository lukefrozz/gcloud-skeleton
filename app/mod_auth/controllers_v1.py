from flask import Blueprint, jsonify, request, render_template
from flask_api import status
from flask_jwt import current_identity, jwt_required
from passlib.hash import pbkdf2_sha512

from app import db, user_datastore, envia_email
from app.mod_auth.models import User

from sqlalchemy import or_
from datetime import datetime, timedelta
from uuid import uuid4
import jwt

mod_auth_v1 = Blueprint('auth_v1', __name__, url_prefix='/v1.0/auth')


@mod_auth_v1.route('/register', methods=['POST'])
def register_post():
    return jsonify(
        msg="Sua conta foi criada com sucesso! "
            "Verifique seu e-mail para ativar a conta (não esqueça de verificar a sua caixa de SPAM).",
        data=request.json
    ), status.HTTP_201_CREATED


@mod_auth_v1.route('/activate/<int:_id>', methods=['GET'])
def activate_get(_id: int):

    return jsonify(
        msg="Conta ativada! Você já pode acessar a sua conta.",
        redirect_to_login=True
    ), status.HTTP_202_ACCEPTED


@mod_auth_v1.route('/recoverpassword', methods=['POST'])
def recover_password_get():
    return jsonify(
        msg="Uma mensagem com o link de recuperação de senha chegará em seu email. " +
            "Se não estiver em sua caixa de entrada, não deixe de verificar a caixa de spam."
    ), status.HTTP_202_ACCEPTED


@mod_auth_v1.route('/changelostpassword', methods=['POST'])
def change_lost_password():
    return jsonify(
        msg='Sua senha foi alterada. Faça login para acessar seus investimentos.'
    )


@mod_auth_v1.route('/changesecret', methods=['POST'])
@jwt_required()
def change_password():
    user: User = current_identity
    data = request.json

    if not pbkdf2_sha512.verify(data['old'], user.password):
        return jsonify(
            msg='Digite sua senha atual.',
            type_err=2
        ), status.HTTP_406_NOT_ACCEPTABLE
    else:
        user.password = pbkdf2_sha512.hash(data['new'])
        user_datastore.commit()
        return jsonify(
            msg='Senha alterada.'
        ), status.HTTP_202_ACCEPTED
