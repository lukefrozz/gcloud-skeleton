Olá, {{name}}.

Você solicitou uma troca de senha em sua conta.

Copie o link abaixo e cole no seu navegador de preferência

https://app.stockinvestmoney.com/recoverpasswd?t={{token}}

Se voê não pediu para trocar de senha, apenas ignore essa mensagem.

Sock Invest Money.